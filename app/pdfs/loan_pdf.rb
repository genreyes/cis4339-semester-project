class LoanPdf< Prawn::Document
  attr_accessor :loan
  def initialize(loan)
    super :page_size => "A4", :page_layout => :landscape
    self.loan = loan
    loan_title
    loan_items
  end
  #sale_info
  # class SalePdf< Prawn::Document
  #
  #   def initialize(sale)
  #     super :page_size => "A4", :page_layout => :landscape
  #     @order = order
  #     sale_title
  #      sale_items
  # sale_info
  #   end
  def loan_title
    text "Loan", size: 25, style: :bold

  end
  # def sale_info
  #  text "Sale ID: #{@sale.id}"
  #  text "Sales Person: #{@sale.salesperson}"
  #  text "Sale Person's Phone: #{@sale.salesperson_phone}"
  #  text "Customer Name: #{@sale.customer_name}"
  #  text "Customer Last Name: #{sale.customer_lastname}"
  #  text "Car Model: #{@sale.car_model}"
  #  text "Car Vin: #{@sale.car_vin}"
  #  text "Car Color: #{@sale.car_color}"
  #  text "Car Price: #{@sale.car_price}"
  #  text "Car Sales Tax: #{@sale.car_sales_tax}"
  #  text "Car Markup: #{@sale.car_markup}"
  #  text "Car Total Price: #{@sale.car_total_price}"
  def loan_items

    move_down 20
    # line_item_rows
    table line_item_rows do
      row(0).font_style= :bold
      self.header = true
      self.row_colors = ["F0F0F0", "FFFFCC"]

    end
  end
  def line_item_rows

    [["Period","Payment","Principal","Interest", "Loan Balance"]] +

        @loan.map do |loan|
          [loan.periods, loan.payment, loan.interest_rate, loan.balance]


        end
  end




end