class SalePdf< Prawn::Document
  attr_accessor :sale
  def initialize(sale)
    super :page_size => "A4", :page_layout => :landscape
    self.sale = sale
    sale_title
     sale_items
    end
    #sale_info
# class SalePdf< Prawn::Document
#
#   def initialize(sale)
#     super :page_size => "A4", :page_layout => :landscape
#     @order = order
#     sale_title
#      sale_items
# sale_info
#   end
  def sale_title
    text "Sale", size: 25, style: :bold

  end
  # def sale_info
  #  text "Sale ID: #{@sale.id}"
  #  text "Sales Person: #{@sale.salesperson}"
  #  text "Sale Person's Phone: #{@sale.salesperson_phone}"
  #  text "Customer Name: #{@sale.customer_name}"
  #  text "Customer Last Name: #{sale.customer_lastname}"
  #  text "Car Model: #{@sale.car_model}"
  #  text "Car Vin: #{@sale.car_vin}"
  #  text "Car Color: #{@sale.car_color}"
  #  text "Car Price: #{@sale.car_price}"
  #  text "Car Sales Tax: #{@sale.car_sales_tax}"
  #  text "Car Markup: #{@sale.car_markup}"
  #  text "Car Total Price: #{@sale.car_total_price}"

  def sale_items

      move_down 20
     # line_item_rows
      table line_item_rows do
        row(0).font_style= :bold
        self.header = true
        self.row_colors = ["F0F0F0", "FFFFCC"]

      end
  end
  def line_item_rows

    [["Sale ID","Sales Person","Sale Person's Phone","Customer Name","Customer Last Name","Car Model", "Car Make",
      "Car Vin","Car Color","Car Price","Car Sales Tax","Car Markup","Car Total Price"]] +
      @sale.map do |sale|
      [sale.id, sale.salesperson,sale.salesperson_phone,sale.customer_name,sale.customer_lastname,sale.car_model,
      sale.car_make,sale.car_vin,sale.car_color,sale.car_price,sale.car_sales_tax,sale.car_markup,
      sale.car_total_price]

     end
  end
  end

