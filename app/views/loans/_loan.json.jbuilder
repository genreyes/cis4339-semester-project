json.extract! loan, :id, :principal, :interest_rate, :periods, :payment_type, :created_at, :updated_at
json.url loan_url(loan, format: :json)
