json.extract! sale, :id, :salesperson, :salesperson_phone, :customer_name, :customer_lastname, :car_model, :car_make, :car_vin, :car_color, :car_price, :car_sales_tax, :car_markup, :car_total_price, :created_at, :updated_at
json.url sale_url(sale, format: :json)
