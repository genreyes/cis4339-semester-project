json.extract! car, :id, :model, :color, :maker, :year, :price, :created_at, :updated_at
json.url car_url(car, format: :json)
