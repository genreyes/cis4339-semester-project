class Car < ApplicationRecord

  has_one_attached :image

  after_commit :add_default_image, on: [:create, :update]


  def self.search(search)
    if search
      where('color LIKE ? OR model LIKE ? OR year LIKE ? OR price LIKE ? OR vin LIKE ? OR maker LIKE ?',  "%#{search}%",  "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      all
    end
  end

  def add_default_image
    unless image.attached?
      self.image.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'car-flat.png')),
                        filename: 'car-flat.png', content_type: 'image/png')
    end


  end


end
