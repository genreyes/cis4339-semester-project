class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      user ||= User.new # guest user (not logged in)
      if user.role =='dealership_owner'
        can :manage, :all
      else
        can :read, Car
      end
        if user.role=='sales_person'
         can :read, Car
         can :read, Customer
          can :manage, Sale
          end
    if user.role=='inventory_manager'
          can :manage, Car
      end
     if user.role=='sales_manager'
          can :manage, Sale

     end
      if user.role=='finance_manager'
          can :manage, Loan
          can :manage, Sale
          end


      #end

    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
           end
           end

