class Loan < ApplicationRecord

  attr_accessor :calculation

  def amortization
    @calculation  = Hash.new
    @calculation[:payments] = Array.new(periods)
    @calculation[:total] = (payment(principal, interest_rate, payment_type, periods).round(2))
    @calculation[:periods] = Array.new(periods)
    @calculation[:principals] = Array.new(periods)
    @calculation[:interests] = Array.new(periods)
    @calculation[:balances] = Array.new(periods)

    #Initial conditions
    @calculation[:payments][0] = 0
    @calculation[:principals][0] = 0
    @calculation[:interests][0] = 0
    @calculation[:balances][0] = principal

    #Inserting data into Arrays
    for i in (1..periods)
      if i != periods
        @calculation[:payments][i] = payment(principal, interest_rate, payment_type, periods)
      else
        @calculation[:payments][i] = payment(principal, interest_rate, payment_type, periods)
      end
      @calculation[:interests][i] = ((@calculation[:balances][i-1]).to_f * (interest_rate/payment_type_yearly_frecuency(payment_type))).round(2)
      @calculation[:principals][i] = (@calculation[:payments][i] - @calculation[:interests][i]).round(2)
      if i == periods
        @calculation[:balances][i] = (@calculation[:balances][i-1] - @calculation[:payments][i] + @calculation[:interests][i]).round(0)
      else
        @calculation[:balances][i] = (@calculation[:balances][i-1] - @calculation[:payments][i] + @calculation[:interests][i]).round(2)
      end

    end

    # def payment_type_yearly_frequency(payment_type)
    #   case payment_type
    #   when 1 then 12
    #   when 2 then 24
    #   when 3 then 26
    #   when 4 then 54
    #   else 12
    #   end
    # end

    # # Return fixed payment during all loan
    # def payment(principal, interest_rate, payment_type, periods)
    #   (principal * ( (interest_rate/payment_type_yearly_frecuency(payment_type)/(1-((1+interest_rate/payment_type_yearly_frecuency(payment_type))**(periods * -1)))) )).round(2)
    # end

  end

  def payment(principal, interest_rate, payment_type, periods)
    (principal * ( (interest_rate/payment_type_yearly_frecuency(payment_type)/(1-((1+interest_rate/payment_type_yearly_frecuency(payment_type))**(periods * -1)))) )).round(2)
  end

  def payment_type_yearly_frecuency(payment_type)
    case payment_type
    when 1 then 12
    when 2 then 24
    when 3 then 26
    when 4 then 54
    else 12
    end
  end


  # def add (principal, interest_rate, periods, payment_type)
  #   puts  " values ---------"
  #   puts  principal
  #   puts interest_rate
  #   puts  "---------"
  #   @total = principal + periods + interest_rate + payment_type
  # end

  # def payment(periods)
  #   payments = []
  #   for periods in 1..periods do
  #     payments.push(periods)
  #   end
  #   payments
  #
  #  #  for 12 months  do
  #  #     payment =(principal * ( (interest_rate/12/(1-((1+interest_rate/12)**(periods * -1)))) )).round(2)
  #  #  payments.push(payment)
  #  #  end
  #  #  payments
  # end

  # def payment(principal, interest_rate, periods)
  #   months = periods * 12
  #   payment = []
  #   for months in 1..months do
  #     payment.push(months)
  #     # payments = (principal * ( (interest_rate/(periods)/(1-((1+interest_rate/(periods))**(periods * -1)))) )).round(2)
  #     # payment.push(payments)
  #   end
  #   payment
  # end



end
