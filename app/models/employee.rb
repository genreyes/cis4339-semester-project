class Employee < ApplicationRecord

  def self.search(search)
    if search
      where('name LIKE ? OR last_name LIKE ? OR role LIKE ? OR email LIKE ? OR phone LIKE ? OR address LIKE ?', "%#{search}%",  "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      all
    end
  end


end
