class SalesController < ApplicationController
  before_action :set_sale, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /sales
  # GET /sales.json
  def index
    authorize! :index, @sale
    @sales = Sale.all
    #puts @sales
    respond_to do |format|
      format.html
      format.pdf do

        pdf = SalePdf.new(@sales)
        send_data pdf.render, filename: "Sales.pdf", type: "application/pdf",disposition: "inline"
        #wickedpdf
        #render pdf: "Sales.pdf", template: "sales_controller/index.html.erb" # layout: 'layouts/pdf.html.erb'



      end
      end

   end
  # GET /sales/1
  # GET /sales/1.json
  def show
    authorize! :show, @sale



      end


  # GET /sales/new
  def new
    @sale = Sale.new
  end

  # GET /sales/1/edit
  def edit
    authorize! :edit, @sale
  end

  # POST /sales
  # POST /sales.json
  def create
    authorize! :create, @sale
    @sale = Sale.new(sale_params)

    respond_to do |format|
      if @sale.save
        format.html { redirect_to @sale, notice: 'Sale was successfully created.' }
        format.json { render :show, status: :created, location: @sale }
      else
        format.html { render :new }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales/1
  # PATCH/PUT /sales/1.json
  def update
    authorize! :update, @sale
    respond_to do |format|
      if @sale.update(sale_params)
        format.html { redirect_to @sale, notice: 'Sale was successfully updated.' }
        format.json { render :show, status: :ok, location: @sale }
      else
        format.html { render :edit }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales/1
  # DELETE /sales/1.json
  def destroy
    authorize! :destroy, @sale
    @sale.destroy
    respond_to do |format|
      format.html { redirect_to sales_url, notice: 'Sale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sale
      @sale = Sale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sale_params
      params.require(:sale).permit(:salesperson, :salesperson_phone, :customer_name, :customer_lastname, :car_model, :car_make, :car_vin, :car_color, :car_price, :car_sales_tax, :car_markup, :car_total_price)
    end
  end


