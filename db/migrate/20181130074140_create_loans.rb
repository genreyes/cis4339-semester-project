class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.float :principal
      t.float :interest_rate
      t.integer :periods
      t.integer :payment_type

      t.timestamps
    end
  end
end
