class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.string :salesperson
      t.string :salesperson_phone
      t.string :customer_name
      t.string :customer_lastname
      t.string :car_model
      t.string :car_make
      t.string :car_vin
      t.string :car_color
      t.float :car_price
      t.float :car_sales_tax
      t.float :car_markup
      t.float :car_total_price

      t.timestamps
    end
  end
end
