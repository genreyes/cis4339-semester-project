class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.string :maker
      t.integer :year
      t.float :price

      t.timestamps
    end
  end
end
