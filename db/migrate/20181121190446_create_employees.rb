class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :address
      t.string :phone

      t.timestamps
    end
  end
end
