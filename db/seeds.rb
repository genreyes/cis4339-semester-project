# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

50.times do |i|
  Car.create(model: "Malibu", maker: "Chevrolet", color: Faker::Vehicle.color, year: "2015", price: 50.00, vin: Faker::Vehicle.vin )
end

# 10.times do |i|
#   Customer.create(first_name: Faker::Name.first_name, last_name: Faker::)
# end