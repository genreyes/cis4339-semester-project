Rails.application.routes.draw do
  devise_for :users
  resources :loans
  resources :sales
  resources :employees
  resources :users
  resources :cars
  resources :customers
  root to: "cars#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
