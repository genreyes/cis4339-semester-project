require 'test_helper'

class SaleTest < ActiveSupport::TestCase
  setup do
    @sale = Sale.new
  end

  test 'sales total = 0 if wholesale' do
    sale = @sale.car_total(0)
    assert_equal(0, sale)
  end

  test 'calculate car total' do
    sale = @sale.car_total(2000)
    assert_equal(2257.052, sale)
  end


  # test "the truth" do
  #   assert true
  # end
end
