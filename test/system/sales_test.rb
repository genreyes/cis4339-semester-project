require "application_system_test_case"

class SalesTest < ApplicationSystemTestCase
  setup do
    @sale = sales(:one)
  end

  test "visiting the index" do
    visit sales_url
    assert_selector "h1", text: "Sales"
  end

  test "creating a Sale" do
    visit sales_url
    click_on "New Sale"

    fill_in "Car Color", with: @sale.car_color
    fill_in "Car Make", with: @sale.car_make
    fill_in "Car Markup", with: @sale.car_markup
    fill_in "Car Model", with: @sale.car_model
    fill_in "Car Price", with: @sale.car_price
    fill_in "Car Sales Tax", with: @sale.car_sales_tax
    fill_in "Car Total Price", with: @sale.car_total_price
    fill_in "Car Vin", with: @sale.car_vin
    fill_in "Customer Lastname", with: @sale.customer_lastname
    fill_in "Customer Name", with: @sale.customer_name
    fill_in "Salesperson", with: @sale.salesperson
    fill_in "Salesperson Phone", with: @sale.salesperson_phone
    click_on "Create Sale"

    assert_text "Sale was successfully created"
    click_on "Back"
  end

  test "updating a Sale" do
    visit sales_url
    click_on "Edit", match: :first

    fill_in "Car Color", with: @sale.car_color
    fill_in "Car Make", with: @sale.car_make
    fill_in "Car Markup", with: @sale.car_markup
    fill_in "Car Model", with: @sale.car_model
    fill_in "Car Price", with: @sale.car_price
    fill_in "Car Sales Tax", with: @sale.car_sales_tax
    fill_in "Car Total Price", with: @sale.car_total_price
    fill_in "Car Vin", with: @sale.car_vin
    fill_in "Customer Lastname", with: @sale.customer_lastname
    fill_in "Customer Name", with: @sale.customer_name
    fill_in "Salesperson", with: @sale.salesperson
    fill_in "Salesperson Phone", with: @sale.salesperson_phone
    click_on "Update Sale"

    assert_text "Sale was successfully updated"
    click_on "Back"
  end

  test "destroying a Sale" do
    visit sales_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sale was successfully destroyed"
  end
end
