require 'test_helper'

class SalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sale = sales(:one)
  end

  test "should get index" do
    get sales_url
    assert_response :success
  end

  test "should get new" do
    get new_sale_url
    assert_response :success
  end

  test "should create sale" do
    assert_difference('Sale.count') do
      post sales_url, params: { sale: { car_color: @sale.car_color, car_make: @sale.car_make, car_markup: @sale.car_markup, car_model: @sale.car_model, car_price: @sale.car_price, car_sales_tax: @sale.car_sales_tax, car_total_price: @sale.car_total_price, car_vin: @sale.car_vin, customer_lastname: @sale.customer_lastname, customer_name: @sale.customer_name, salesperson: @sale.salesperson, salesperson_phone: @sale.salesperson_phone } }
    end

    assert_redirected_to sale_url(Sale.last)
  end

  test "should show sale" do
    get sale_url(@sale)
    assert_response :success
  end

  test "should get edit" do
    get edit_sale_url(@sale)
    assert_response :success
  end

  test "should update sale" do
    patch sale_url(@sale), params: { sale: { car_color: @sale.car_color, car_make: @sale.car_make, car_markup: @sale.car_markup, car_model: @sale.car_model, car_price: @sale.car_price, car_sales_tax: @sale.car_sales_tax, car_total_price: @sale.car_total_price, car_vin: @sale.car_vin, customer_lastname: @sale.customer_lastname, customer_name: @sale.customer_name, salesperson: @sale.salesperson, salesperson_phone: @sale.salesperson_phone } }
    assert_redirected_to sale_url(@sale)
  end

  test "should destroy sale" do
    assert_difference('Sale.count', -1) do
      delete sale_url(@sale)
    end

    assert_redirected_to sales_url
  end
end
